package org.thelhonir.bussiness;

import org.junit.Test;
import org.thelhonir.bussiness.impl.PlotServiceImpl;
import org.thelhonir.domainmodel.Plot;
import org.thelhonir.util.PlotIdComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by dagomez on 30/03/16.
 */
public class PlotServiceTest {

    private PlotService plotService;
    @Test
    public void getPlotByIdTest(){
        try {
            //Arrange
            plotService = new PlotServiceImpl();
            int plotId = 0;
            Plot expectedPlot = assembleTestPlot();
            Plot actualPlot;
            //Act
            actualPlot = plotService.getPlotById(plotId);
            //Assert
            assertEquals(expectedPlot.toString(), actualPlot.toString());
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test
    public void getPlotIdByCoordinatesTest(){
        try {
            //Arrange
            plotService = new PlotServiceImpl();
            int coordX = 0;
            int coordY = 0;
            int expectedPlotId = assembleTestPlot().getPlotId();
            int actualPlotId;
            //Act
            actualPlotId = plotService.getPlotIdByCoordinates(coordX, coordY);
            //Assert
            assertEquals(expectedPlotId, actualPlotId);
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test
    public void getNearestPlotByDirectionToTest(){
        try {
            //Arrange
            plotService = new PlotServiceImpl();
            int expectedPlotId = assembleTestPlot().getPlotId();
            int actualPlotId;
            /** Plot ID under expected Plot */
            int originPlotId = 1;
            //Act
            actualPlotId = plotService.getNearestPlotByDirectionTo(originPlotId, Direction.UP);
            //Assert
            assertEquals(expectedPlotId, actualPlotId);
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test
    public void getPlotsInRangeFromTest(){
        try {
            //Arrange
            plotService = new PlotServiceImpl();
            List<Plot> expectedPlots = assembleExpectedPlots();
            List<Plot> actualPlots;
            int originPlotId = 0;
            int range = 1;
            //Act
            actualPlots = plotService.getPlotsInRangeFrom(originPlotId, range);
            Collections.sort(actualPlots, new PlotIdComparator());
            //Assert
            for (int i = 0; i < expectedPlots.size(); i++) {
                assertEquals(expectedPlots.get(i).getPlotId(), actualPlots.get(i).getPlotId());
            }
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    private List<Plot> assembleExpectedPlots() {
        List<Plot> plots = new ArrayList<>();
        plots.add(new Plot(0, "plot1", 0,0));
        plots.add(new Plot(1, "plot2", 0,1));
        plots.add(new Plot(5, "plot6", 1,0));
        plots.add(new Plot(6, "plot7", 1,1));
        Collections.sort(plots, new PlotIdComparator());
        return plots;
    }

    private Plot assembleTestPlot() {
        return new Plot(0, "plot1", 0, 0);
    }


}
