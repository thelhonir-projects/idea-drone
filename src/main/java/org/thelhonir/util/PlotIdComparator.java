package org.thelhonir.util;

import org.thelhonir.domainmodel.Plot;

import java.util.Comparator;

/**
 * Created by dagomez on 30/03/16.
 */
public class PlotIdComparator implements Comparator<Plot> {
    @Override
    public int compare(Plot o1, Plot o2) {
        Integer o1PlotId = Integer.valueOf(o1.getPlotId());
        Integer o2PlotId = Integer.valueOf(o2.getPlotId());
        return o1PlotId.compareTo(o2PlotId);
    }
}
