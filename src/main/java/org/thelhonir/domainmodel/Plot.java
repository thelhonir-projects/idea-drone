package org.thelhonir.domainmodel;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by dagomez on 30/03/16.
 */
public class Plot {
    private int plotId;
    private String plotName;
    private int coordinateX;
    private int coordinateY;

    public Plot(int plotId, String plotName, int coordinateX, int coordinateY) {
        this.setCoordinateX(coordinateX);
        this.setCoordinateY(coordinateY);
        this.setPlotId(plotId);
        this.setPlotName(plotName);
    }

    public Plot() {
        super();
    }

    public int getPlotId() {
        return plotId;
    }

    public void setPlotId(int plotId) {
        this.plotId = plotId;
    }

    public String getPlotName() {
        return plotName;
    }

    public void setPlotName(String plotName) {
        this.plotName = plotName;
    }

    public int getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
    }

    public int getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
    }

    @Override
    public String toString() {
        StringBuilder plot2Str = new StringBuilder();
        plot2Str.append("PLOT ID: " + getPlotId() + "\n");
        plot2Str.append("PLOT NAME: " + getPlotName() + "\n");
        plot2Str.append("COORD X: " + getCoordinateX() + "\n");
        plot2Str.append("COORD Y: " + getCoordinateY() + "\n");
        return plot2Str.toString();
    }
}
