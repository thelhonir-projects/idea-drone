package org.thelhonir.repositories.mock.impl;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.thelhonir.domainmodel.Plot;
import org.thelhonir.repositories.mock.PlotRepository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by dagomez on 30/03/16.
 */
public class PlotRepositoryImpl implements PlotRepository {

    private static final String MOCK_JSON_FILE_PATH = "/mock/plots.JSON";

    /** JSON CONSTANT FIELDS */
    private static final String PLOT_LIST = "plotList";
    private static final String PLOT_NAME = "plotName";
    private static final String PLOT_ID = "plotId";
    private static final String COORD_X = "coordinateX";
    private static final String COORD_Y = "coordinateY";


    @Override
    public Plot getPlotByPlotId(int plotId) {
        Integer plotIdObj = Integer.valueOf(plotId);
        JSONObject jsonObject = parseJsonFile();
        JSONArray jsonArray = (JSONArray) jsonObject.get(PLOT_LIST);
        for (Object obj : jsonArray) {
            JSONObject plot = (JSONObject) obj;
            if (StringUtils.equals(plot.get(PLOT_ID).toString(), plotIdObj.toString())) {
                return assemblePlotObj(plot);
            }
        }
        return null;
    }

    @Override
    public int getPlotIdByCoordinates(int coordX, int coordY){
        Integer coordXObj = Integer.valueOf(coordX);
        Integer coordYObj = Integer.valueOf(coordY);
        JSONObject jsonObject = parseJsonFile();
        JSONArray jsonArray = (JSONArray) jsonObject.get(PLOT_LIST);
        for (Object obj : jsonArray) {
            JSONObject plot = (JSONObject) obj;
            if (StringUtils.equals(plot.get(COORD_X).toString(), coordXObj.toString()) && StringUtils.equals(plot.get(COORD_Y).toString(), coordYObj.toString()) ) {
                Plot returnPlot =  assemblePlotObj(plot);
                return returnPlot.getPlotId();
            }
        }
        return -1;
    }

    private JSONObject parseJsonFile() {
        JSONParser jsonParser = new JSONParser();
        try {
            return (JSONObject) jsonParser.parse(new BufferedReader(new InputStreamReader(
                    this.getClass().getResourceAsStream(MOCK_JSON_FILE_PATH))));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Plot assemblePlotObj(JSONObject obj) {
        int resultPlotId = Integer.parseInt(obj.get(PLOT_ID).toString());
        String resultPlotName = obj.get(PLOT_NAME).toString();
        int resultCoordX = Integer.parseInt(obj.get(COORD_X).toString());
        int resultCoordY = Integer.parseInt(obj.get(COORD_Y).toString());
        return new Plot(resultPlotId, resultPlotName, resultCoordX, resultCoordY);
    }
}
