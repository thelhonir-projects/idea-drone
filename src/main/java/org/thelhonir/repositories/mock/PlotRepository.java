package org.thelhonir.repositories.mock;

import org.thelhonir.domainmodel.Plot;

/**
 * Created by dagomez on 30/03/16.
 */
public interface PlotRepository {
    Plot getPlotByPlotId(int plotId);
    int getPlotIdByCoordinates(int coordX, int coordY);
}
