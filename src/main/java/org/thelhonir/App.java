package org.thelhonir;

import org.thelhonir.bussiness.PlotService;
import org.thelhonir.bussiness.impl.PlotServiceImpl;
import org.thelhonir.domainmodel.Plot;
import org.thelhonir.util.PlotIdComparator;

import java.util.Collections;
import java.util.List;

/**
 * Hello world!
 */
public class App {
    private static PlotService plotService;

    public static void main(String[] args) {
        /** SEE TEST TO CHECK EXECUTION */
        plotService = new PlotServiceImpl();
        int plotId = plotService.getPlotIdByCoordinates(2, 2);
        List<Plot> plots = plotService.getPlotsInRangeFrom(plotId, 1);
        Collections.sort(plots, new PlotIdComparator());
        for (Plot plot : plots) {
            System.out.println(plot);
        }
    }
}
