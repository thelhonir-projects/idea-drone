package org.thelhonir.bussiness.impl;

import org.thelhonir.bussiness.Direction;
import org.thelhonir.bussiness.PlotService;
import org.thelhonir.domainmodel.Plot;
import org.thelhonir.repositories.mock.PlotRepository;
import org.thelhonir.repositories.mock.impl.PlotRepositoryImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dagomez on 30/03/16.
 */
public class PlotServiceImpl implements PlotService {
    private PlotRepository plotRepository;

    @Override
    public Plot getPlotById(int plotId) {
        plotRepository = new PlotRepositoryImpl();
        return plotRepository.getPlotByPlotId(plotId);
    }

    @Override
    public int getPlotIdByCoordinates(int coordinateX, int coordinateY) {
        plotRepository = new PlotRepositoryImpl();
        return plotRepository.getPlotIdByCoordinates(coordinateX, coordinateY);
    }

    @Override
    public int getNearestPlotByDirectionTo(int plotId, Direction direction) {
        Plot originPlot = getPlotById(plotId);
        return direction.move(originPlot, 1).getPlotId();
    }

    @Override
    public List<Plot> getPlotsInRangeFrom(int plotId, int range) {
        List<Plot> plotsInRange = new ArrayList<Plot>();
        Plot originPlot = getPlotById(plotId);
        List<Plot> upperPlots = getPlotsInDirection(originPlot, Direction.UP, range);
        List<Plot> lowerPlots = getPlotsInDirection(originPlot, Direction.DOWN, range);
        plotsInRange.add(originPlot);
        plotsInRange.addAll(getPlotsInDirection(originPlot, Direction.LEFT, range));
        plotsInRange.addAll(getPlotsInDirection(originPlot, Direction.RIGHT, range));
        plotsInRange.addAll(upperPlots);
        plotsInRange.addAll(lowerPlots);
        plotsInRange.addAll(getHorizontalPlots(upperPlots, range));
        plotsInRange.addAll(getHorizontalPlots(lowerPlots, range));
        return plotsInRange;
    }

    private List<Plot> getPlotsInDirection(Plot originPlot, Direction direction, int range) {
        List<Plot> plots = new ArrayList<Plot>();
        for (int i = 1; i <= range; i++) {
            Plot plot = direction.move(originPlot, i);
            if (null != plot && plots.indexOf(plot) == -1) {
                plots.add(plot);
            }
        }
        return plots;
    }

    private List<Plot> getHorizontalPlots(List<Plot> plots, int range){
        List<Plot> plotsInRange = new ArrayList<Plot>();
        for (Plot plot : plots) {
            plotsInRange.addAll(getPlotsInDirection(plot, Direction.LEFT, range));
            plotsInRange.addAll(getPlotsInDirection(plot, Direction.RIGHT, range));
        }
        return plotsInRange;
    }


}
