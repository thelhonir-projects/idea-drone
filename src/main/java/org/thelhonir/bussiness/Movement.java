package org.thelhonir.bussiness;

import org.thelhonir.domainmodel.Plot;

/**
 * Created by dagomez on 30/03/16.
 */
public interface Movement {
    Plot move(Plot originPlot, int range);
}
