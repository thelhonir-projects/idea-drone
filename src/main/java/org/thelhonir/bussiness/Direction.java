package org.thelhonir.bussiness;

import org.thelhonir.bussiness.impl.PlotServiceImpl;
import org.thelhonir.domainmodel.Plot;

/**
 * Created by dagomez on 30/03/16.
 */
public enum Direction implements Movement {

    UP("UP") {
        @Override
        public Plot move(Plot originPlot, int range) {
            PlotService plotService = new PlotServiceImpl();
            int plotId = plotService.getPlotIdByCoordinates(originPlot.getCoordinateX(), (originPlot.getCoordinateY() - range));
            return plotService.getPlotById(plotId);
        }
    },
    DOWN("DOWN") {
        @Override
        public Plot move(Plot originPlot, int range) {
            PlotService plotService = new PlotServiceImpl();
            int plotId = plotService.getPlotIdByCoordinates(originPlot.getCoordinateX(), (originPlot.getCoordinateY() + range));
            return plotService.getPlotById(plotId);
        }
    },
    RIGHT("RIGHT") {
        @Override
        public Plot move(Plot originPlot, int range) {
            PlotService plotService = new PlotServiceImpl();
            int plotId = plotService.getPlotIdByCoordinates((originPlot.getCoordinateX() + range), originPlot.getCoordinateY());
            return plotService.getPlotById(plotId);
        }
    },
    LEFT("LEFT") {
        @Override
        public Plot move(Plot originPlot, int range) {
            PlotService plotService = new PlotServiceImpl();
            int plotId = plotService.getPlotIdByCoordinates((originPlot.getCoordinateX() - range), originPlot.getCoordinateY());
            return plotService.getPlotById(plotId);
        }
    };

    private String direction;
    Direction(String direction) {
        this.direction = direction;
    }
}
