package org.thelhonir.bussiness;

import org.thelhonir.domainmodel.Plot;

import java.util.List;

/**
 * Created by dagomez on 30/03/16.
 */
public interface PlotService {
    Plot getPlotById(int plotId);
    int getPlotIdByCoordinates(int coordinateX, int coordinateY);
    int getNearestPlotByDirectionTo(int plotId, Direction direction);
    List<Plot> getPlotsInRangeFrom(int plotId, int range);
}
